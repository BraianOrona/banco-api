package com.folcademy.bancoorona.models.dto;

import com.folcademy.bancoorona.models.enums.AccountType;

public class AccountDetailedDTO {

    private Long number;
    private String cbu;
    private AccountType type;

    public AccountDetailedDTO() {
    }

    public AccountDetailedDTO(Long number, String cbu, AccountType type) {
        this.number = number;
        this.cbu = cbu;
        this.type = type;
    }

    public Long getNumber() {
        return number;
    }

    public String getCbu() {
        return cbu;
    }

    public AccountType getType() {
        return type;
    }
}
