package com.folcademy.bancoorona.models.dto;

import java.math.BigDecimal;

public class TransactionsByIdDTO {
    private String date;
    private String description;
    private BigDecimal amount;
    private String currency;
    private AccountDTO from;
    private AccountDTO to;

    public TransactionsByIdDTO() {
    }

    public TransactionsByIdDTO(String description, BigDecimal amount, String currency, AccountDTO origin, AccountDTO destination, String date) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = origin;
        this.to = destination;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountDTO getFrom() {
        return from;
    }

    public AccountDTO getTo() {
        return to;
    }
}
