package com.folcademy.bancoorona.models.dto;

public class UserDTO {

    private String dni;
    private String firstName;
    private String lastName;
    private String adress;

    public UserDTO() {
    }

    public UserDTO(String dni, String firstName, String lastName, String adress) {
        this.dni = dni;
        this.firstName = firstName;
        this.lastName = lastName;
        this.adress = adress;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAdress() {
        return adress;
    }
}
