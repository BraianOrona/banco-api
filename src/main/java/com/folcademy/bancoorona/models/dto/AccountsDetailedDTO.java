package com.folcademy.bancoorona.models.dto;

import java.util.List;

public class AccountsDetailedDTO {
    private List<AccountDetailedDTO> accounts;

    public AccountsDetailedDTO() {
    }

    public AccountsDetailedDTO(List<AccountDetailedDTO> accounts) {
        this.accounts = accounts;
    }

    public List<AccountDetailedDTO> getAccounts() {
        return accounts;
    }
}
