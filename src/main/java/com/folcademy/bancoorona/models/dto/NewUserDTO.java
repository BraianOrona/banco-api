package com.folcademy.bancoorona.models.dto;

public class NewUserDTO {

    public String password;
    private String dni;
    private String firstName;
    private String lastName;
    private String adress;
    public String username;

    public NewUserDTO() {
    }

    public NewUserDTO(String dni, String firstName, String lastName, String adress,String username, String password) {
        this.dni = dni;
        this.firstName = firstName;
        this.lastName = lastName;
        this.adress = adress;
        this.password = password;
        this.username = username;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getUsername() {
        return username;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAdress() {
        return adress;
    }

    public String getPassword() {
        return password;
    }
}
