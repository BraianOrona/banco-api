package com.folcademy.bancoorona.models.enums;

public enum CurrencyType {
    ARS, USD, EUR
}
