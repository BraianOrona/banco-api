package com.folcademy.bancoorona.models.repositories;

import com.folcademy.bancoorona.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AccountRepository extends CrudRepository<AccountEntity, Long> {
    List<AccountEntity> findAllByUserId(String userId);
    AccountEntity findByNumber(Long number);
}
