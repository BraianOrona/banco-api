package com.folcademy.bancoorona.models.repositories;

import com.folcademy.bancoorona.models.entities.TransactionsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionsRepository extends CrudRepository<TransactionsEntity, Long> {
    List<TransactionsEntity> findAllByOriginOrDestination(String accountNumber, String accountNumber1);

}
