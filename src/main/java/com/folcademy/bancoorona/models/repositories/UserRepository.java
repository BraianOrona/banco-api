package com.folcademy.bancoorona.models.repositories;

import com.folcademy.bancoorona.models.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    UserEntity findByDni(String dni);
    Optional<UserEntity> findByUsername(String username);

}
