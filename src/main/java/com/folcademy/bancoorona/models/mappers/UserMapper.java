package com.folcademy.bancoorona.models.mappers;

import com.folcademy.bancoorona.models.dto.NewUserDTO;
import com.folcademy.bancoorona.models.dto.UserByDniDTO;
import com.folcademy.bancoorona.models.entities.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserByDniDTO mapUserEntityToUserDTO (UserEntity userEntity){
        return new UserByDniDTO(userEntity.getDni(), userEntity.getFirstName(), userEntity.getLastName(), userEntity.getAdress());
    }

    public UserEntity mapUserDTOToUserEntity (NewUserDTO newUserDTO){
        return new UserEntity(newUserDTO.getDni(),newUserDTO.getFirstName(),newUserDTO.getLastName(),newUserDTO.getAdress(), newUserDTO.getUsername(), newUserDTO.getPassword());
    }

}
