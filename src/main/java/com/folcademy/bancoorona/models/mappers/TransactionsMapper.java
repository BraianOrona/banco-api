package com.folcademy.bancoorona.models.mappers;

import com.folcademy.bancoorona.models.dto.NewTransactionDTO;
import com.folcademy.bancoorona.models.entities.TransactionsEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TransactionsMapper {

    public TransactionsEntity mapTransactionDtoToTransactionEntity (NewTransactionDTO newTransactionDTO){
        return new TransactionsEntity(new Date(), newTransactionDTO.getDescription(), newTransactionDTO.getAmount(), newTransactionDTO.getCurrency(), newTransactionDTO.getFrom(), newTransactionDTO.getTo());
    }
}
