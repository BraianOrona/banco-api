package com.folcademy.bancoorona.models.mappers;

import com.folcademy.bancoorona.models.dto.AccountDetailedDTO;
import com.folcademy.bancoorona.models.dto.NewAccountDTO;
import com.folcademy.bancoorona.models.entities.AccountEntity;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

    public AccountDetailedDTO mapAccountEntityToAccountDTO (AccountEntity accountEntity){
        return new AccountDetailedDTO(accountEntity.getNumber(), accountEntity.getCbu(), accountEntity.getType());
    }

    public AccountEntity mapAccountDTOToAccountEntity (NewAccountDTO newAccountDTO){
        return new AccountEntity(newAccountDTO.getNumber(),newAccountDTO.getCbu(), newAccountDTO.getType(), newAccountDTO.getUserId());
    }
}
