package com.folcademy.bancoorona.models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "transactions")
public class TransactionsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date date;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String origin;
    private String destination;

    public TransactionsEntity() {
    }

    public TransactionsEntity(Date date, String description, BigDecimal amount, String currency, String origin, String destination) {

        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }

    public Long getId() {
        return id;
    }


    public Date getDate(){
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }
}
