package com.folcademy.bancoorona.models.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "users")
public class UserEntity {

    @Id
    private String dni;
    private String firstName;
    private String lastName;
    private String adress;
    private String username;
    private String password;

    public UserEntity() {
    }

    public UserEntity(String dni, String firstName, String lastName, String adress, String password, String username) {
        this.dni = dni;
        this.firstName = firstName;
        this.lastName = lastName;
        this.adress = adress;
        this.password = password;
        this.username = username;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAdress() {
        return adress;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String fullName(){
        return firstName + " " + lastName;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
