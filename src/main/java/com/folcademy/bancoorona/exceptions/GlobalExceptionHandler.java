package com.folcademy.bancoorona.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> defaultErrorHandler(HttpServletRequest request, Exception e){
        e.printStackTrace();
        return new ResponseEntity<ErrorMessage>(
                new ErrorMessage("Error generico",
                        e.getMessage(),
                        "1",
                        request.getRequestURI()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> notFoundHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<ErrorMessage>(
                new ErrorMessage("Not found",
                        e.getMessage(),
                        "2",
                        request.getRequestURI()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({BadRequestException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> badRequestExceptions(HttpServletRequest request, Exception e){
        return new ResponseEntity<ErrorMessage>(
                new ErrorMessage("Bad request",
                        e.getMessage(),
                        "3",
                        request.getRequestURI()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({NotAuthorizedException.class, UsernameNotFoundException.class, BadCredentialsException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> notAuthorizedExceptionsHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<ErrorMessage>(
                new ErrorMessage("Not authorized",
                        e.getMessage(),
                        "4",
                        request.getRequestURI()),
                HttpStatus.UNAUTHORIZED);
    }
}
