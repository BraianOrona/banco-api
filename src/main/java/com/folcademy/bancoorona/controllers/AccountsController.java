package com.folcademy.bancoorona.controllers;

import com.folcademy.bancoorona.models.dto.AccountsDetailedDTO;
import com.folcademy.bancoorona.models.dto.NewAccountDTO;
import com.folcademy.bancoorona.services.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
public class AccountsController {


    @Autowired
    private AccountsService accountsService;



    @GetMapping("/{userId}")
    public ResponseEntity<AccountsDetailedDTO> getUserAccounts(@PathVariable String userId){
        return new ResponseEntity<>(accountsService.getAccount(userId), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> newAccount(@RequestBody NewAccountDTO newAccountDTO){
        return accountsService.newAccount(newAccountDTO);
    }
}
