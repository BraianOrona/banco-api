package com.folcademy.bancoorona.controllers;

import com.folcademy.bancoorona.models.dto.NewTransactionDTO;
import com.folcademy.bancoorona.models.dto.TransactionsByIdDTO;
import com.folcademy.bancoorona.models.dto.TransactionsDTO;
import com.folcademy.bancoorona.models.entities.TransactionsEntity;
import com.folcademy.bancoorona.models.repositories.TransactionsRepository;
import com.folcademy.bancoorona.services.TransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    private TransactionsRepository transactionsRepository;
    //service
    @Autowired
    private TransactionsService transactionsService;


    @GetMapping("/index")
    public List<TransactionsEntity> index(){
        return (List<TransactionsEntity>) transactionsRepository.findAll();
    }

    //service
    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionsDTO> getTransactionsForAccount(@PathVariable Long accountNumber){
        return new ResponseEntity<>(transactionsService.getTransactions(accountNumber), HttpStatus.OK);
    }

    //service 2
    @GetMapping ("/{transactionId}")
    public ResponseEntity<TransactionsByIdDTO> getTransactionsForId(@PathVariable Long transactionId){
        return new ResponseEntity<>(transactionsService.getTransactionsById(transactionId), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        transactionsRepository.deleteById(id);
        return "Se elimino correctamente";
    }


    //mapping
    @PostMapping("/new")
    public ResponseEntity<String> createTransaction(@RequestBody NewTransactionDTO newTransactionDTO){
        return transactionsService.createTransactions(newTransactionDTO);
    }


}
