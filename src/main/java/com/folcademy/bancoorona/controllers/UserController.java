package com.folcademy.bancoorona.controllers;

import com.folcademy.bancoorona.models.dto.NewUserDTO;
import com.folcademy.bancoorona.models.dto.UserByDniDTO;
import com.folcademy.bancoorona.models.repositories.UserRepository;
import com.folcademy.bancoorona.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @GetMapping("/{dni}")
    public ResponseEntity<UserByDniDTO> getUser (@PathVariable String dni){
        return new ResponseEntity<>(userService.getUser(dni), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> newUser(@RequestBody NewUserDTO newUserDTO){
        return userService.newUser(newUserDTO);
    }

    @DeleteMapping("/{dni}")
    public String deleteUser(@PathVariable String dni){
        userService.deleteUser(dni);
        return "Se elimino correctamente";
    }
}
