package com.folcademy.bancoorona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoOronaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoOronaApplication.class, args);
	}

}
