package com.folcademy.bancoorona.services;

import com.folcademy.bancoorona.exceptions.BadRequestException;
import com.folcademy.bancoorona.exceptions.NotFoundException;
import com.folcademy.bancoorona.models.dto.AccountDetailedDTO;
import com.folcademy.bancoorona.models.dto.AccountsDetailedDTO;
import com.folcademy.bancoorona.models.dto.NewAccountDTO;
import com.folcademy.bancoorona.models.entities.AccountEntity;
import com.folcademy.bancoorona.models.mappers.AccountMapper;
import com.folcademy.bancoorona.models.repositories.AccountRepository;
import com.folcademy.bancoorona.models.repositories.TransactionsRepository;
import com.folcademy.bancoorona.models.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountsService {

    private final AccountRepository accountRepository;
    private final TransactionsRepository transactionsRepository;
    private final UserRepository userRepository;
    private final AccountMapper accountMapper;


    public AccountsService(AccountRepository accountRepository, TransactionsRepository transactionsRepository, UserRepository userRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.transactionsRepository = transactionsRepository;
        this.userRepository = userRepository;
        this.accountMapper = accountMapper;
    }

    public AccountsDetailedDTO getAccount(String userId){
        List<AccountEntity> accountEntities = accountRepository.findAllByUserId(userId);
        List<AccountDetailedDTO> accountsDTOList = new ArrayList<>();

        if (accountEntities.isEmpty()){
            throw new NotFoundException("El usuario no posee cuentas");
        }

        for (AccountEntity accountEntity: accountEntities){
            AccountDetailedDTO accountDetailedDTO = accountMapper.mapAccountEntityToAccountDTO(accountEntity);
            accountsDTOList.add(accountDetailedDTO);
        }
        return new AccountsDetailedDTO(accountsDTOList);
    }

    public ResponseEntity<String> newAccount(NewAccountDTO newAccountDTO){

        if (newAccountDTO.getCbu().isBlank()){
            throw new BadRequestException("Error al crear la cuenta, debe ingresar su cbu");
        }

        if (newAccountDTO.getCbu().length() != 20){
            throw new BadRequestException("Error al crear la cuenta, su cbu tiene que tener 20 digitos");
        }

        if (newAccountDTO.getUserId().isBlank() ){
            throw new BadRequestException("Error al crear la cuenta, debe ingresar el user id");
        }
        
        try{
            AccountEntity accountEntity = accountMapper.mapAccountDTOToAccountEntity(newAccountDTO);
            accountRepository.save(accountEntity);
            return new ResponseEntity<>("Cuenta creada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Algo salio mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
