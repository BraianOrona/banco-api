package com.folcademy.bancoorona.services;

import com.folcademy.bancoorona.exceptions.BadRequestException;
import com.folcademy.bancoorona.exceptions.NotFoundException;
import com.folcademy.bancoorona.models.dto.NewUserDTO;
import com.folcademy.bancoorona.models.dto.UserByDniDTO;
import com.folcademy.bancoorona.models.entities.UserEntity;
import com.folcademy.bancoorona.models.mappers.UserMapper;
import com.folcademy.bancoorona.models.repositories.AccountRepository;
import com.folcademy.bancoorona.models.repositories.TransactionsRepository;
import com.folcademy.bancoorona.models.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final UserMapper userMapper;


    public UserService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UserRepository userRepository, UserMapper userMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    //GET

    public UserByDniDTO getUser (String dni){
        if (!userRepository.existsById(dni)) throw new NotFoundException("No existe el usuario con este dni");

        UserEntity userEntities = userRepository.findByDni(dni);
        UserByDniDTO userByDniDTO = new UserByDniDTO();
        userByDniDTO = new UserByDniDTO(userEntities.getDni(),
                userEntities.getFirstName(),
                userEntities.getLastName(),
                userEntities.getAdress());

        return userByDniDTO;

    }
    //POST

    public ResponseEntity<String> newUser(NewUserDTO newUserDTO){

        if (newUserDTO.getDni().isBlank()){
            throw new BadRequestException("Error al crear el usuario, debe ingresar su dni");
        }

        if (newUserDTO.getFirstName().isBlank()){
            throw new BadRequestException("Error al crear el usuario, debe ingresar su nombre");
        }

        if (newUserDTO.getLastName().isBlank() ){
            throw new BadRequestException("Error al crear el usuario, debe ingresar su apellido");
        }
        if (newUserDTO.getAdress().isBlank() ){
            throw new BadRequestException("Error al crear el usuario, debe ingresar su direccion");
        }

        try{
            UserEntity userEntity = userMapper.mapUserDTOToUserEntity(newUserDTO);
            userRepository.save(userEntity);
            return new ResponseEntity<>("Usuario creado", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Algo salio mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    //DELETE
    public ResponseEntity<String> deleteUser (String dni){
        if (!userRepository.existsById(dni)){
            throw new BadRequestException("Error al eliminar usuario, el dni es incorrecto");
        }
        userRepository.deleteById(dni);
        return new ResponseEntity<>("Usuario eliminado", HttpStatus.OK);
    }


}
