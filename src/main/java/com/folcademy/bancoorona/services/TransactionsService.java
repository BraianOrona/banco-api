package com.folcademy.bancoorona.services;

import com.folcademy.bancoorona.exceptions.BadRequestException;
import com.folcademy.bancoorona.exceptions.NotFoundException;
import com.folcademy.bancoorona.models.dto.*;
import com.folcademy.bancoorona.models.dto.TransactionsDTO;
import com.folcademy.bancoorona.models.dto.TransactionDTO;
import com.folcademy.bancoorona.models.dto.NewTransactionDTO;
import com.folcademy.bancoorona.models.entities.AccountEntity;
import com.folcademy.bancoorona.models.entities.TransactionsEntity;
import com.folcademy.bancoorona.models.entities.UserEntity;
import com.folcademy.bancoorona.models.mappers.TransactionsMapper;
import com.folcademy.bancoorona.models.repositories.AccountRepository;
import com.folcademy.bancoorona.models.repositories.TransactionsRepository;
import com.folcademy.bancoorona.models.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionsService {

    private final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final TransactionsMapper transactionsMapper;

    public TransactionsService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UserRepository userRepository, TransactionsMapper transactionsMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionsMapper = transactionsMapper;
    }

    public TransactionsDTO getTransactions(Long accountNumber){
        List<TransactionsEntity> transactionsEntities = transactionsRepository.findAllByOriginOrDestination(accountNumber.toString(),accountNumber.toString());
        List<TransactionDTO> transactionDTOList = new ArrayList<>();


        for (TransactionsEntity transactionsEntity: transactionsEntities){
            AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));
            UserEntity toUserEntity = userRepository.findByDni(toAccountEntity.getUserId());

            String transactionType;

            if (transactionsEntity.getOrigin().equals(accountNumber.toString())){
                transactionType = "GASTO";
            }else {
                transactionType = "INGRESO";
            }

            transactionDTOList.add(
                    new TransactionDTO(transactionsEntity.getDate().toString(),
                            transactionsEntity.getDescription(),
                            transactionsEntity.getAmount(),
                            transactionsEntity.getCurrency().toString(),
                            transactionsEntity.getOrigin(),
                            toUserEntity.fullName() + "/ CBU: " + toAccountEntity.getCbu(),
                            transactionType)
            );
        }
        BigDecimal balance = BigDecimal.ZERO;
        for (TransactionDTO transactionDTO : transactionDTOList){
            if (transactionDTO.getType().equals("GASTO")){
                balance = balance.subtract(transactionDTO.getAmount());
            }else {
                balance = balance.add(transactionDTO.getAmount());
            }
        }

        return new TransactionsDTO(transactionDTOList, balance);
    }

    //---------//

    public TransactionsByIdDTO getTransactionsById(Long id){
        Optional<TransactionsEntity> transactionsEntityOptional = transactionsRepository.findById(id);
        if (transactionsEntityOptional.isEmpty()){
            throw new NotFoundException("No existe la transaccion");
        }
        TransactionsEntity transactionEntity = transactionsEntityOptional.get();
        TransactionsByIdDTO transactionDTO = new TransactionsByIdDTO();

        AccountEntity fromAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionEntity.getOrigin()));
        UserEntity fromUserEntity = userRepository.findByDni(fromAccountEntity.getUserId());
        AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionEntity.getDestination()));
        UserEntity toUserEntity = userRepository.findByDni(toAccountEntity.getUserId());
        AccountDTO from = new AccountDTO(fromUserEntity.getFirstName(), fromUserEntity.getLastName(),fromAccountEntity.getCbu());
        AccountDTO to = new AccountDTO(toUserEntity.getFirstName(),toUserEntity.getLastName(),toAccountEntity.getCbu());

        transactionDTO = new TransactionsByIdDTO(
                transactionEntity.getDescription(),
                transactionEntity.getAmount(),
                transactionEntity.getCurrency(),
                from,
                to,
                transactionEntity.getDate().toString());
        return transactionDTO;
    }

    public ResponseEntity<String> createTransactions(NewTransactionDTO newTransactionDTO){

        if (newTransactionDTO.getFrom().equals(newTransactionDTO.getTo() )){
            throw new BadRequestException("Error al crear la transacción, la cuenta de origen y destino son las mismas");
        }

        if (newTransactionDTO.getAmount().compareTo(BigDecimal.ZERO) <= 0){
            throw new BadRequestException("Error al crear la transacción, el monto no puede ser igual a 0 o negativo");
        }

        if (newTransactionDTO.getTo().equals("")){
            throw new BadRequestException("Error al crear la transacción, debe ingresar la cuenta de destino");
        }

        if (!accountRepository.existsById(Long.parseLong(newTransactionDTO.getTo()))){
            throw new BadRequestException("Error al crear la transacción, la cuenta de destino no existe");
        }

        if (newTransactionDTO.getFrom().equals("")){
            throw new BadRequestException("Error al crear la transacción, debe ingresar la cuenta de origen");
        }

        if (!accountRepository.existsById(Long.parseLong(newTransactionDTO.getFrom()))){
            throw new BadRequestException("Error al crear la transacción, la cuenta de origen no existe");
        }

        try{
            TransactionsEntity transactionsEntity = transactionsMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
            transactionsRepository.save(transactionsEntity);
            return new ResponseEntity<>("Transaccion creada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Algo salio mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
