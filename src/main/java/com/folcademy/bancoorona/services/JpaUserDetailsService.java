package com.folcademy.bancoorona.services;

import com.folcademy.bancoorona.models.entities.UserEntity;
import com.folcademy.bancoorona.models.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userEntityOptional = userRepository.findByUsername(username);

        if (userEntityOptional.isEmpty()){
            throw new UsernameNotFoundException("Not found: "+ username);
        }

        UserEntity userEntity = userEntityOptional.get();

        return new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>());
    }
}
